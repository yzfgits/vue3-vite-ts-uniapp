import { defineConfig } from "vite";
import uni from "@dcloudio/vite-plugin-uni";

const InitConfig = require('./script/initConfig');

// https://vitejs.dev/config/
export default defineConfig({
  define: {
    'process.env': {
    }
  },
  resolve: {
    alias:{
      '@': 'src'
    }
  },
  plugins: [
    uni(),
    new InitConfig()
  ],
});
