// 获取小程序信息
const accountInfo = uni.getAccountInfoSync();
const { appId } = accountInfo.miniProgram;
const prefixeAgean = '';// 总的前缀
const fcrmC = '/mlab-c/c';
const prefix_applet = '/api-applet-c';
const prefix_operation = '/api-operation-c';
const prefix_member = '/api-member-c';// 会员
const prefix_coupon = '/api-coupon-c';// 卡券
const { VUE_APP_BASE_URL_BIGDATA_C, VUE_APP_BASE_URL } = process.env;
const app = {
  // 新的微信授权
  getAuth: `/mlab-c/wx/user/${appId}/token`,
  // 授权后存取手机号
  setAuth: `/mlab-c/wx/user/${appId}/auth`,
  // 授权地理位置后调用的门店接口
  getStore: `/mlab-c/wx/user/${appId}/store`,
  // 授权后存取手机号
  setPhone: `/mlab-c/wx/user/${appId}/phone`,
};
const common = {
  queryHouseCase: '/mlab-c/c/houseCase/queryHouseCase',
  queryHouseCaseDetail: '/mlab-c/c/houseCase/queryHouseCaseDetail',
  houseArea: '/mlab-c/c/houseCase/houseArea',
  houseStyle: '/mlab-c/c/houseCase/houseStyle',
  houseRoom: '/mlab-c/c/houseCase/houseRoom',
  houseBudget: '/mlab-c/c/houseCase/houseBudget',
  houseCaseFavority: '/mlab-c/c/houseCase/houseCaseFavority',
  queryFavoriteHouseCase: '/mlab-c/c/houseCase/queryFavoriteHouseCase',
  queryItemList: '/mlab-c/c/construction/queryItemList',
  queryMeItemList: '/mlab-c/c/construction/queryMeItemList',
  constructionLike: '/mlab-c/c/construction/updateWatch',
  queryWatchList: '/mlab-c/c/construction/queryWatchList',
  queryCosDetails: '/mlab-c/c/construction/queryDetails',
  queryFreeQuotation: '/mlab-c/c/house/freeQuotation',
  queryGetStorePhone: '/mlab-c/c/house/getStorePhone',
  queryReservationMeasurement: '/mlab-c/c/house/reservationMeasurement',
  queryStylist: '/mlab-c/c/stylist/query',
  queryStyDetails: '/mlab-c/c/stylist/details',
  queryStyCase: '/mlab-c/c/stylist/queryCase',
  noticeSend: '/mlab-c/c/notice/send',
  // 获取去过的门店
  getStores: '/mlab-c/c/store/getStoreListByMemberId',
  // 生成分享码和任务详情
  createShareCode: '/mlab-c/c/share/fission/activity/sharecode',
};
const home = {
  // 在线商城首页轮播图
  onlineMallBanner: '/mlab-c/c/banner/query',
  // 店铺列表
  getAllStoreList: '/mlab-c/c/store/getAllStoreList',
  // 获取潮逛指南列表
  getCmsContent: `${prefixeAgean}${fcrmC}/information/query`,
};
const active = {
  getActiveList: '/mlab-c/c/applyActivity/getApplyActivities',
  // 报名活动详情
  getActiveDetail: '/mlab-c/c/applyActivity/getApplyActivityDetail',
  // 验证会员活动是否已参与
  activeIsJoin: '/mlab-c/c/memberApply/verifyMember',
  getSignUpData: '/mlab-c/c/memberApply/getMemberApplyStatus',
  // 会员活动报名
  signUpActive: '/mlab-c/c/memberApply/addMemberApply',
  fissionActivityList: '/mlab-c/c/fissionActivity/list',
  fissionInvite: '/mlab-c/c/fissionActivity/invite',
  fissionInviteInfo: '/mlab-c/c/fissionActivity/fissionActivityInvite/list',
  generateCode: '/mlab-c/c/miniAppCode/generateCode',
  joinFissionActive: '/mlab-c/c/fissionActivity/participateIn',
  getActiveFissionDetail: '/mlab-c/c/fissionActivity/fissionActivityInfo',
  // 家居活动信息
  activityInfo: '/mlab-c/c/share/fission/activity/info',

};
const shoppingGuide = {
  getSalesManList: '/mlab-c/c/salesMan/getSalesManList',
  // 获取导购详情
  getSalesDetail: '/mlab-c/c/salesMan/get',
  // 专属导购的接口
  getAssignedSalesMan: '/mlab-c/c/memberInfo/assignedSalesMan'
};
const other = {
  // 查询咨询详情
  informationId: '/mlab-c/c/information/get',
  // 获取企业文化
  getCulture: '/mlab-c/c/enterprise/getExistEnterprise',
  // 获取回放信息
  getMpScene: '/mlab-c/c/share/fission/getSceneValue',

};
const card = {
  // 我的-- 查看券详情
  memberCouponDetail: '/mlab-c/c/coupon/memberCouponDetail',
  // 我的--卡券列表
  memberCouponList: '/mlab-c/c/coupon/memberCouponQuery',
  // 超值领券购买
  buyCoupon: `${prefixeAgean}${prefix_coupon}/c/couponMall/buyCoupon`,
};
const goods = {
  likeSave: '/mlab-c/c/favorite/goods/save',
  // 查询团购状态
  queryByGroupCode: '/mlab-c/c/group/queryByGroupCode',
  // 在线商城详情页
  onlineMallDetail: '/mlab-c/c/goods/get',
  // 商品页面的案例
  goodsCase: '/mlab-c/c/caseExample/queryGoods',
};
const order = {
  // 确认收货
  receiptGoods: '/mlab-c/c/order/receipt',
  // 重新支付
  repayOrder: '/mlab-c/c/order/repay',
  // 取消订单
  cancelOrder: '/mlab-c/c/order/cancel',
  // 确认支付
  passPay: '/mlab-c/c/b/order/passPay',
  // 导购订单列表
  querySalesOrder: '/mlab-c/c/b/order/queryByPage',
  // 获取订单列表页
  getOrderList: '/mlab-c/c/order/pageList',
  // 订单详情
  queryOrderDetail: '/mlab-c/c/order/detail',
  // 收货
  receiptOrder: '/mlab-c/c/order/receipt',
  // 查购物信息
  getShippingInfo: '/mlab-c/c/order/getShippingInfo',
  // 查询用户收货地址
  findMemberAddress: '/mlab-c/c/mallMemberAddress/findMemberAddress',
  // 专属导购的接口
  getAssignedSalesMan: '/mlab-c/c/memberInfo/assignedSalesMan',
  // 提交订单
  createOrder: '/mlab-c/c/order/create',
  // 导购开单
  billOrder: '/mlab-c/c/b/order/billing',
  joinSuperGroup: '/mlab-c/c/group/joinSuperGroup',
  // 加入团购
  joinGroup: '/mlab-c/c/group/joinGroup',

  cancelGroup: '/mlab-c/c/pay/cancelPay',
  // 获取经销商仓库地址列表
  queryDealerAddress: '/mlab-c/c/dealerAddress/queryDealerAddress',
  // 支付结果查询
  queryPay: '/mlab-c/c/pay/queryPay',
  // 根据地址id查询地址详情
  getOneAddress: '/mlab-c/c/mallMemberAddress',
  // 新增编辑收货地址
  updateOneAddress: '/mlab-c/c/mallMemberAddress/insertOrUpdate',
  // 删除收货地址
  deleteAddress: '/mlab-c/c/mallMemberAddress/deleteMemberAddress',
};
const period = {
  // c申请分销商
  distributionPartnerApply: '/mlab-c/c/distributionPartner/apply',
  // 分销商查询
  distributionPartnerQuery: '/mlab-c/c/distributionPartner/query',
  // /账户明细
  distributionqueryDetail: '/mlab-c/c/distributionPartner/queryDetail',
  // 我的信息保存
  distributionSaveInfo: `/mlab-c/c/distributionPartner/saveInfo`,
  //  查询推广商品列表
  queryPromotionGoods: `/mlab-c/c/distributionPartner/queryPromotionGoods`,
  // 查询我的推广
  queryMyPromotion: `/mlab-c/c/distributionPartner/queryMyPromotion`,
  // 查询我的订单
  queryMyOrders: `/mlab-c/c/distributionPartner/queryMyOrders`,
  //    领取商品
  receiveGoods: `/mlab-c/c/distributionPartner/receiveGoods`,
  //
  applyCash: `/mlab-c/c/distributionPartner/applyCash`,
};
// 剩余没有归类的家居API，后期西需求手动归类下
const noClass = {
  // 上传头像
  uploadAvatar: `${VUE_APP_BASE_URL}${prefix_operation}/v1/file/upload`,
  saveAvatar: `${prefixeAgean}${prefix_applet}/v1/c/wx/user/uploadHeadImgUrl`,
  getAvatar: `${prefixeAgean}${prefix_applet}/v1/c/wx/user/getHeadImgUrl`,
  // 获取店铺关联用户
  getIMMembers: '/mlab-c/chat/im/getIMMembers',
  // 融云初始化token
  initIMChat: '/mlab-c/chat/im/initIMChat',
  // 融云获取target用户信息
  getIMChat: '/mlab-c/chat/im/getIMChat',
  getVipInfo: `${prefixeAgean}${prefix_member}/c/member/findMemberDetail`,
  getImUserInfo: '/mlab-c/chat/im/initIMChat',
  // 新的微信授权
  getAuth: `/mlab-c/wx/user/${appId}/token`,
  // 授权后存取手机号
  setAuth: `/mlab-c/wx/user/${appId}/auth`,
  buyCoupon: `${prefixeAgean}${prefix_coupon}/c/couponMall/buyCoupon`, // 超值领券购买
  // 授权后存取手机号
  setPhone: `/mlab-c/wx/user/${appId}/phone`,
  login: `${prefixeAgean}${prefix_applet}/c/login`, // 登录
  registerIntergral: `${prefixeAgean}${prefix_member}/c/integral/manualadjust/adjust`, // 注册积分
  // 获取潮逛指南列表
  getCmsContent: `${prefixeAgean}${fcrmC}/information/query`,
  // 获取企业文化
  getCulture: '/mlab-c/c/enterprise/getExistEnterprise',
  // 获取会员code
  getUserCode: `${prefixeAgean}${prefix_applet}/v1/c/wx/user/findUserByOpenId`,
  // 查询品类
  getCategoryList: '/mlab-c/c/caseExample/queryCaseCategoryToC',
  // 获取企业案例
  getCaseList: '/mlab-c/c/caseExample/queryListToC',
  // 在线商城列表(we家居品类系列双筛选)
  querySeriesCategoryGoods: '/mlab-c/c/goods/querySeriesCategoryGoods',

  // 在线商城首页轮播图
  onlineMallBanner: '/mlab-c/c/banner/query',

  savePerosnInfo: `${prefixeAgean}${prefix_member}/c/member/perfectMemberInfo`,

  getAllInterestTags: `${prefixeAgean}${prefix_member}/c/member/findInterestTag`,

  // 在线商城列表
  onlineMallList: '/mlab-c/c/goods/query',
  // 团购商品列表
  groupActiveList: '/mlab-c/c/group/queryPage',
  // 开团
  creatGroup: '/mlab-c/c/group/createGroup',
  // 加入团购
  joinGroup: '/mlab-c/c/group/joinGroup',
  /* eslint-disable */
   joinSuperGroup: '/mlab-c/c/group/joinSuperGroup',
   // 查询团购状态
   queryByGroupCode: '/mlab-c/c/group/queryByGroupCode',
 
   cancelGroup: '/mlab-c/c/pay/cancelPay',
   // 门店报名活动列表
   getActiveList: '/mlab-c/c/applyActivity/getApplyActivities',
   // 裂变活动列表
   fissionActivityList: '/mlab-c/c/fissionActivity/list',
   fissionInvite: '/mlab-c/c/fissionActivity/invite',
   fissionInviteInfo: '/mlab-c/c/fissionActivity/fissionActivityInvite/list',
   generateCode: '/mlab-c/c/miniAppCode/generateCode',
   joinFissionActive: '/mlab-c/c/fissionActivity/participateIn',
   // 报名活动详情
   getActiveDetail: '/mlab-c/c/applyActivity/getApplyActivityDetail',
   // 报名活动详情
   getActiveFissionDetail: '/mlab-c/c/fissionActivity/fissionActivityInfo',
   // 在线商城列表(we家居)
   onlineMallListWe: '/mlab-c/c/goods/querySeriesGoods',
   // 在线商城详情页
   onlineMallDetail: '/mlab-c/c/goods/get',
   // 授权地理位置后调用的门店接口
   getStore: `/mlab-c/wx/user/${appId}/store`,
   // 查询系列
   getSeries: '/mlab-c/c/series/obtainSeriesIncludeGoods',
   // 查询系列
   queryGoodsInCaseExample: '/mlab-c/c/goods/queryGoodsInCaseExample',
   // 专属导购的接口
   getAssignedSalesMan: '/mlab-c/c/memberInfo/assignedSalesMan',
   // 获取导购详情
   getSalesDetail: '/mlab-c/c/salesMan/get',
   // 点赞
   giveLikeOperation: '/mlab-c/c/salesMan/giveLikeOperation',
   // 卡券列表
   getMemberCoupon: '/mlab-c/c/coupon/query',
   // 获取卡券详情
   getCouponId: '/mlab-c/c/coupon/detail',
   // 领取券
   receiveCoupon: '/mlab-c/c/coupon/receive',
   // 我的-- 查看券详情
   memberCouponDetail: '/mlab-c/c/coupon/memberCouponDetail',
   // 我的--卡券列表
   memberCouponList: '/mlab-c/c/coupon/memberCouponQuery',
   // 我的--帮助与反馈列表
   afterServiceDatabank: '/mlab-c/c/afterServiceDatabank/list',
   // 我的--帮助与反馈列表
   addComplaint: '/mlab-c/c/complaint/add',
   // 设置专属导购
   setSales: '/mlab-c/c/salesMan/createExclusiveSalesMan',
   // b端核销详情
   chargeOffDetail: '/mlab-c/b/coupon/chargeOffDetail',
   // b端券核销
   chargeOff: '/mlab-c/b/coupon/chargeOff',
   // 健全接口
   dataSign: '/mlab-c/c/datareport/sign',
   // 查询导购详情七人头像
   faceImage: `${VUE_APP_BASE_URL_BIGDATA_C}/data-c/api/dataApplication/dataApi/query/v3`,
   // 查询总浏览人数
   totalCheck: `${VUE_APP_BASE_URL_BIGDATA_C}/data-c/api/dataApplication/dataApi/query/v3`,
   // 查询用户收货地址
   findMemberAddress: '/mlab-c/c/mallMemberAddress/findMemberAddress',
   // 新增编辑收货地址
   updateOneAddress: '/mlab-c/c/mallMemberAddress/insertOrUpdate',
   // 删除收货地址
   deleteAddress: '/mlab-c/c/mallMemberAddress/deleteMemberAddress',
   // 根据地址id查询地址详情
   getOneAddress: '/mlab-c/c/mallMemberAddress',
   // 获取去过的门店
   getStores: '/mlab-c/c/store/getStoreListByMemberId',
   // IM聊天获取sig  /mlab-c/chat/im/getUserSig
   getUserSig: '/mlab-c/chat/im/getUserSig',
   // 获取直播列表
   getRoomList: '/mlab-c/c/live/roomList',
   // 获取直播详情
   getRoomInfo: '/mlab-c/c/live/room',
   // 获取回放信息
   getReplay: '/mlab-c/c/live/getReplay',
   // 获取回放信息
   getMpScene: '/mlab-c/c/share/fission/getSceneValue',
   // 获取可用卡券数量
   memberCouponCount: '/mlab-c/c/coupon/memberCouponCount',
   // 提交订单
   createOrder: '/mlab-c/c/order/create',
   // 导购开单
   billOrder: '/mlab-c/c/b/order/billing',
   // 导购订单列表
   querySalesOrder: '/mlab-c/c/b/order/queryByPage',
   // 支付结果查询
   queryPay: '/mlab-c/c/pay/queryPay',
   // 重新支付
   repayOrder: '/mlab-c/c/order/repay',
   // 确认支付
   passPay: '/mlab-c/c/b/order/passPay',
   // 查购物信息
   getShippingInfo: '/mlab-c/c/order/getShippingInfo',
   // 确认收货
   receiptGoods: '/mlab-c/c/order/receipt',
   // 获取订单列表页
   getOrderList: '/mlab-c/c/order/pageList',
   // 取消订单
   cancelOrder: '/mlab-c/c/order/cancel',
   // 订单详情
   queryOrderDetail: '/mlab-c/c/order/detail',
   // 自提商品详情
   goodsChargeOffDetail: '/mlab-c/b/mallGoodsChargeOff/goodsChargeOffDetail',
   // 自提商品核销
   goodsChargeOff: '/mlab-c/b/mallGoodsChargeOff/goodsChargeOff',
   // 收货
   receiptOrder: '/mlab-c/c/order/receipt',
   // 商品页面的案例
   goodsCase: '/mlab-c/c/caseExample/queryGoods',
   // 获取客服资料库列表
   getServerList: '/mlab-c/c/salesManDatabank/query',
   // 获取客服资料库详情
   getServerDetail: `${VUE_APP_BASE_URL}/mlab-c/c/salesManDatabank/get`,
   // 获取我的名片信息
   getBusinessCard: '/mlab-c/c/salesMan/get',
   // 发送订阅消息
   sendSubMsg: '/mlab-c/c/subscribe/msg/sendMsg',
   // 查询名片数据
   getBusinessData: `${VUE_APP_BASE_URL_BIGDATA_C}/data-c/api/dataApplication/dataApi/query/v3`,
   // 编辑名片
   updateCardInfo: '/mlab-c/c/salesMan/updateCardInfo',
   // 上传图片
   uploadImg: '/mlab-c/c/file/upload',
   // 获取品牌欢迎语
   queryOne: '/mlab-c/c/welcomeSpeech/queryOne',
   // 活动信息
   activityInfo: '/mlab-c/c/share/fission/activity/info',
   // 根据经销商ID和品牌ID获取关键词列表
   getBrandKeywords: '/mlab-c/c/brandKeywords/getBrandKeywords',
   // 根据关键词ID获取回复信息
   getKeywordsReplys: '/mlab-c/c/brandKeywords/getKeywordsReplys',
   // 查询咨询详情
   informationId: '/mlab-c/c/information/get',
   // 生成分享码和任务详情
   createShareCode: '/mlab-c/c/share/fission/activity/sharecode',
   // 创建电子券订单
   createCouponOrder: '/mlab-c/c/couponOrder/create',
   getAllStoreList: '/mlab-c/c/store/getAllStoreList',
   // 验证会员活动是否已参与
   activeIsJoin: '/mlab-c/c/memberApply/verifyMember',
   // 获取用户的报名列表
   getMyActive: '/mlab-c/c/memberApply/getMemberApplyActs',
   // 获取会员报名活动信息
   getSignUpData: '/mlab-c/c/memberApply/getMemberApplyStatus',
   // 会员活动报名
   signUpActive: '/mlab-c/c/memberApply/addMemberApply',
   // 根据门店id获取品牌列表
   getBrandsByStoreId: '/mlab-c/c/brand/getBrandsByStoreId',
   // 查询商品信息列表
   getBrandGoods: '/mlab-c/c/goods/getGoodsDetails',
   // 查询商品信息
   getGoodsSku: '/mlab-c/c/goods/get',
   // 补录订单
   suppleOrder: '/mlab-c/c/b/order/replacementOrder',
   // 获取经销商仓库地址列表
   queryDealerAddress: '/mlab-c/c/dealerAddress/queryDealerAddress',
   //喜欢保存接口 /c/favorite/goods/save
   likeSave: '/mlab-c/c/favorite/goods/save',
   // 喜欢列表查询
   likeListAPI: '/mlab-c/c/favorite/goods/list',
   // 喜欢列表删除
   deletelikeList: '/mlab-c/c/favorite/goods/delete',
   // 喜欢售完列表查询
   likeSellOutListAPI:'/mlab-c/c/favorite/goods/listSellOut',
   //首页配置查询
   homeSet:'/mlab-c/c/banner/listIndexModule',
   // 判断用户是否专属导购 和是否入驻一号导购
   judgeGuideBtnJump: '/mlab-c/c/memberInfo/getResult',
    
}
// export default {
//   app,
//   order,
//   home,
//   active,
//   goods,
//   shoppingGuide,
//   common
// };
export {
  app, home, active, shoppingGuide, common, other, card, goods, order,
  period,
  noClass
};
