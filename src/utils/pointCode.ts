const ROOM_MEASUR_T = 9263; // 预约量房
const FREE_PRICE_T = 9266; // 免费报价
const CASE_INFO_T = 9253; // 案例喜欢按钮
const CASE_INFO_N = 9254;// 取消案例喜欢按钮
const VR_INFO_T = 9255; // vr喜欢按钮
const VR_INFO_N = 9256; // 取消vr喜欢按钮
const PROJO_INFO_T = 9257; // 在建工地喜欢按钮
const PROJO_INFO_N = 9258; // 取消在建工地喜欢按钮
const SALESMAN_PAGE_F = 9131; // 分享导购名片

const CASE_PAGE_P = 9222; // 精选案例页面
const CASE_CLICK_F = 9224; // 点击案例
const CASE_INFO_P = 9225; // 案例详情
const CASE_SHARE_F = 9227; // 案例分享

const VR_YANGBAN_P = 9228; // VR样板间
const VR_INFO_F = 9230; // 查看vr详情
const VR_SHARE_F = 9231; // VR样板间分享

const GD_PAGE_P = 9232;// 在建工地
const GD_SERCH_F = 9234;// 搜索工地
const PROJO_CLICK_F = 9235;// 点击项目

const PROJO_INFO_P = 9236;// 查看项目详情
const PROJO_SHARE_F = 9238;// 分享项目

const HX_CHANGE_F = 9241; // 户型选择
const MJ_CHANGE_F = 9242; // 面积选择
const FG_CHANGE_F = 9243; // 风格选择
const YS_CHANGE_F = 9244; // 预算选择

const MY_FITMENT_P = 9245; // 我的装修
const MY_LIKE_P = 9247; // 我的喜欢
const MY_LIKE_SHARE_F = 9248; // 我的喜欢分享

const DESIGN_PAGE_P = 9250; // 设计团队
const DESIGN_VIEW_F = 9252; // 查看设计师主页
const DESIGN_PREVIEW_F = 9270; // 查看设计师主页

const GUIDE_DETAIL_P = 9086; // 导购详情
const GUIDE_CARD_SHARE = 9131; // 导购名片分享

export default {
  ROOM_MEASUR_T,
  FREE_PRICE_T,
  CASE_INFO_T,
  CASE_INFO_N,
  VR_INFO_T,
  VR_INFO_N,
  PROJO_INFO_T,
  PROJO_INFO_N,
  SALESMAN_PAGE_F,
  CASE_PAGE_P,
  CASE_CLICK_F,
  CASE_INFO_P,
  CASE_SHARE_F,
  VR_YANGBAN_P,
  VR_INFO_F,
  VR_SHARE_F,
  GD_PAGE_P,
  GD_SERCH_F,
  PROJO_CLICK_F,
  PROJO_INFO_P,
  PROJO_SHARE_F,
  HX_CHANGE_F,
  MJ_CHANGE_F,
  FG_CHANGE_F,
  YS_CHANGE_F,
  MY_FITMENT_P,
  MY_LIKE_P,
  MY_LIKE_SHARE_F,
  DESIGN_PAGE_P,
  DESIGN_VIEW_F,
  DESIGN_PREVIEW_F,
  GUIDE_DETAIL_P,
  GUIDE_CARD_SHARE,
};
